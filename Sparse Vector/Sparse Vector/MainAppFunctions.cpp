#include "MainAppFunctions.h"

using namespace std;

void vInstruction()
{
	cout << "Sparse vectors are vectors with same value(basic value) on most of their elements // positions(offsets) counting from 0" << endl
		<< "Commands: " << endl << "mvec <len> <def> - creates rare vector with length = <len> and basic value = <def>" << endl
		<< "len <len> - changes rare vector's length to <len>" << endl << "def <off> <val> - sets value <val> in place(offset) <off>" << endl
		<< "get <off> - reads value in <off> and prints it to user" << endl << "print - prints actual status of rare vector" << endl << "del - deletes vector if it exists"
		<< endl << "quit - end the program" << endl;
}
void vDeleteVector(int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements)
{
	vClearVector(pitValues, pitOffsets);
	piBasicValueContainer = NULL;
	piLengthContainer = NULL;
	piNumberOfElements = 0;
}
void vCreateVector(int iLength, int iBasicValue, int &piLengthContainer, int &piBasicValueContainer, int **pitValues, int **pitOffsets, int &piTableLength, int &piNumberOfElements)
{
	if (piNumberOfElements != 0)
	{
		vDeleteVector(pitValues, pitOffsets, piBasicValueContainer, piLengthContainer, piNumberOfElements);
		//if iLengthContainer != NULL then Vector was created before, so it will be deleted
	}
	piBasicValueContainer = iBasicValue;
	piLengthContainer = iLength;

	vCreateTables(pitValues, pitOffsets, I_BASIC_TABLE_LENGTH, piTableLength);

}
void vPrintVector(int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements)
{
	cout << sVectorOnString(iLength, iBasicValue, pitValues, pitOffsets, iNumberOfElements) << endl;
}
void vChangeVectorLength(int iLength, int &piLengthContainer, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength)
{
	//checking offsets;
	for (int i = 0; i < piNumberOfElements; i++)
	{
		if (pitOffsets[i] >= iLength)
		{
			vClearVectorTail(i, pitValues, pitOffsets, piNumberOfElements);
		}
	}
	if (piTableLength > I_DOUBLE_TABLE * piNumberOfElements && piTableLength > I_BASIC_TABLE_LENGTH)
	{
		vChangeTableLength(&pitValues, &pitOffsets, piNumberOfElements, piTableLength, false);
	}
	//change Length
	piLengthContainer = iLength;
}
//void vChangeBasicValue(int iBasicValue, int &piBasicValueContainer, int iNumberOfElements)
//{
//	if (iNumberOfElements == 0)
//	{
//		piBasicValueContainer = iBasicValue;
//	}
//	else
//	{
//		cout << S_CHANGE_BASIC_VALUE_ERROR << endl; //take it to interface
//	}
//}
int iReadValue(int iReadPos, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements)
{
	if (iNumberOfElements > 0)
	{
		int i = 0;
		while (pitOffsets[i] < iReadPos)
		{
			i++;
		}	
		if (pitOffsets[i] == iReadPos)
		{
			return pitValues[i];
		}
		else
		{
			return iBasicValue;
		}
	}
	else
	{
		return iBasicValue;
	}
		
}
void vSetCertainValue(int iValue, int iOffset, int iBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int iLength)
{
	int i = iOffsetContains(iOffset, *pitOffsets, piNumberOfElements);
	if (i == -1)
	{
		if (iValue != iBasicValue)
		{
			vAddSorted(iValue, iOffset, pitValues, pitOffsets, piNumberOfElements, piTableLength);
		}
		// else do nothing
	}
	else
	{
		if (iValue != iBasicValue)
		{
			(*pitValues)[i] = iValue;
			(*pitOffsets)[i] = iOffset;
		}
		else
		{
			vDeleteElement(i, *pitValues, *pitOffsets, piNumberOfElements, piTableLength);
		}
	}
}


