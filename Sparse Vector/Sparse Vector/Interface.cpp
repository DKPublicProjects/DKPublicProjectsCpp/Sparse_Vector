#include "Interface.h"

using namespace std;

int iSplitString(string sContainer, string *psSplitTable)
{
	int i_nrOfElem = 0;
	std::string s = sContainer;
	size_t pos = 0;
	std::string token;

	while ((pos = s.find(S_SPACE)) != std::string::npos) {
		token = s.substr(0, pos);
		if (i_nrOfElem < I_MAXIMAL_SPLIT_STRINGS)
		{
			psSplitTable[i_nrOfElem] = token;
		}
		i_nrOfElem++;
		s.erase(0, pos + S_SPACE.length());
	}
	if (i_nrOfElem < I_MAXIMAL_SPLIT_STRINGS)
	{	
		psSplitTable[i_nrOfElem] = s;
	}
	i_nrOfElem++;

	return i_nrOfElem;
}
int getArg (string sArgContainer, int &iValid)
{
	int i_arg = 0;
	bool b_negative = false;
	char c_temp = sArgContainer.at(0);
	unsigned int i = 0;
	if (c_temp == I_ASCII_MINUS)
	{
		b_negative = true;
		i++;
	}
	while(i < sArgContainer.length() && iValid==I_NO_ERROR)
	{
		c_temp = sArgContainer.at(i);
		if (c_temp >= I_ASCII_ZERO && c_temp <= I_ASCII_NINE)
		{
			i_arg = (i_arg*I_SHIFT_NUMBER) + (c_temp - I_ASCII_ZERO);
			i++;
		}
		else
		{
			iValid = I_WRONG_COMMAND;
		}
	}
	return !b_negative ? i_arg : i_arg*(-1);
}
void vSendMessage(int iMessageId, int iPrintNumber)
{
	switch (iMessageId)
	{
		//cases
		case I_WRONG_COMMAND: 
			cout << S_WRONG_COMMAND << endl;
			break;
		case I_WRONG_ARGUMENTS:
			cout << S_WRONG_ARGS << endl;
			break;
		case I_VECTOR_NOT_EXISTS: 
			cout << S_CREATE_VECTOR << endl;
			break;
		case  I_NO_ERROR:
			break;
		case I_VECTOR_DELETED:
			cout << S_VECTOR_DELETED << endl;
			break;
		case I_QUIT_MESSAGE:
			cout << S_QUIT_MESSAGE << endl;
			break;
		case I_HELP:
			cout << S_HELP_END << endl;
			break;
		case I_LENGTH_CHANGED:
			cout << S_LENGTH_CHANGED << endl;
			break;
		case I_DELETED_EXISTING_AND_CREATED:
			cout << S_VECTOR_DELETED << endl << S_VECTOR_CREATED << endl;
			break;
		case I_VECTOR_CREATED:
			cout << S_VECTOR_CREATED << endl;
			break;
		case I_VALUE_SET:
			cout << S_VALUE_SET << endl;
			break;
		case I_VALUE:
			cout << S_VALUE << iPrintNumber << endl;
	}
}
bool bNoArgFunctions(bool &bVectorCreated, string *pstSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId)
{
	if (pstSplited[I_FUNCTION] == S_PRINT)
	{
		iMessageId = iPrint(bVectorCreated, piVectorLength, piBasicValue, *pitValues, *pitOffsets, piNumberOfElements);
	}
	else if (pstSplited[I_FUNCTION] == S_DEL)
	{
		iMessageId = iDelete(bVectorCreated, pitValues, pitOffsets, piBasicValue, piVectorLength, piNumberOfElements);
	}
	else if (pstSplited[I_FUNCTION] == S_QUIT)
	{
		iMessageId = iQuit(bVectorCreated, pitValues, pitOffsets, piBasicValue, piVectorLength, piNumberOfElements);
		return true;
	}
	else if (pstSplited[I_FUNCTION] == S_HELP)
	{
		iMessageId = iHelpMessage();
	}
	else
	{
		iMessageId = I_WRONG_COMMAND;
	}
	return false;
}
void vOneArgFunctions(int &iPrintNumber, bool &bVectorCreated, string *stSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId)
{
	int i_arg1 = getArg(stSplited[I_ARG_1], iMessageId);
	if (iMessageId == 0)
	{
		if (stSplited[I_FUNCTION] == S_LEN)
		{
			iMessageId = iChangeLength(bVectorCreated, i_arg1, piVectorLength, *pitValues, *pitOffsets, piNumberOfElements, piTableLength);
		}
		else if (stSplited[I_FUNCTION] == S_GET)
		{
			iMessageId = iPrintValue(iPrintNumber, bVectorCreated, i_arg1, piBasicValue, *pitValues, *pitOffsets, piNumberOfElements, piVectorLength);
		}
		else
		{
			iMessageId = I_WRONG_COMMAND;
		}
	}
}
void vTwoArgFunctions(bool &bVectorCreated, string *stSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId)
{
	int i_arg1 = getArg(stSplited[I_ARG_1], iMessageId);
	int i_arg2 = getArg(stSplited[I_ARG_2], iMessageId);
	if (iMessageId == 0)
	{
		if (stSplited[I_FUNCTION] == S_MVEC)
		{
			iMessageId = iMakeVector(bVectorCreated, i_arg1, i_arg2, piVectorLength, piBasicValue, pitValues, pitOffsets, piTableLength, piNumberOfElements);
		}
		else if (stSplited[I_FUNCTION] == S_DEF)
		{
			iMessageId = iDefineValue(bVectorCreated, i_arg2, i_arg1, piBasicValue, pitValues, pitOffsets, piNumberOfElements, piTableLength, piVectorLength);
		}
		else
		{
			iMessageId = I_WRONG_COMMAND;
		}
	}
}
void vCommandLineInterface(int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength)
{
	bool b_end = false;
	int i_switcher = 0;
	string s_container;
	string *ps_splitTable = new string[I_MAXIMAL_SPLIT_STRINGS];
	bool b_vectorCreated = false;
	int i_nrOfStrings = 0;
	int i_messageId = I_NO_ERROR;
	int i_printNumber = 0;
	while (!b_end)
	{

		getline(cin, s_container);
		i_nrOfStrings = iSplitString(s_container, ps_splitTable);
		i_messageId = I_NO_ERROR;

		switch (i_nrOfStrings)
		{
		case I_NO_ARG_FUNCTIONS:
			b_end = bNoArgFunctions(b_vectorCreated, ps_splitTable, piVectorLength, piBasicValue, pitValues, pitOffsets, piNumberOfElements, piTableLength,i_messageId);
		break;

		case I_ONE_ARG_FUNCTIONS:
			vOneArgFunctions(i_printNumber, b_vectorCreated, ps_splitTable, piVectorLength, piBasicValue, pitValues, pitOffsets, piNumberOfElements, piTableLength, i_messageId);
		break;

		case I_TWO_ARG_FUNCTIONS:
			vTwoArgFunctions(b_vectorCreated, ps_splitTable, piVectorLength, piBasicValue, pitValues, pitOffsets, piNumberOfElements, piTableLength, i_messageId);
		break;

		default:
			i_messageId = I_WRONG_COMMAND;
		}
		vSendMessage(i_messageId, i_printNumber);
	}
	delete[] ps_splitTable;
}
void vRun()
{
	int i_realTableLength;
	int *pit_values;
	int *pit_offsets;
	int i_vectorLength = NULL;
	int i_basicValue = NULL;
	int i_nrOfElem = 0;
	vCommandLineInterface(i_vectorLength, i_basicValue, &pit_values, &pit_offsets, i_nrOfElem, i_realTableLength);
}
