#pragma once

#include <iostream>
#include "AdditionalFunctions.h"
#include "MainAppFunctions.h"
#include "Const.h"
#include <string>


void vInstruction();
void vDeleteVector(int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements);
void vCreateVector(int iLength, int iBasicValue, int &piLengthContainer, int &piBasicValueContainer, int **pitValues, int **pitOffsets, int &piTableLength, int &piNumberOfElements);
void vPrintVector(int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements);
void vChangeVectorLength(int iLength, int &piLengthContainer, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength);
//void vChangeBasicValue(int iBasicValue, int &piBasicValueContainer, int iNumberOfElements);
int iReadValue(int iReadPos, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements);
void vSetCertainValue(int iValue, int iOffset, int iBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int iLength);