#include "PublicFunctions.h"

using namespace std;

int iPrint(bool bVectorCreated, int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements)
{
	if (bVectorCreated)
	{
		vPrintVector(iLength, iBasicValue, pitValues, pitOffsets, iNumberOfElements);
	}
	else
	{
		return I_VECTOR_NOT_EXISTS;
	}
	return I_NO_ERROR;
}
int iDelete(bool &bVectorCreated, int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements)
{
	if (bVectorCreated) 
	{
		vDeleteVector(pitValues, pitOffsets, piBasicValueContainer, piLengthContainer, piNumberOfElements);
		bVectorCreated = false;
	}
	else
	{
		return I_VECTOR_NOT_EXISTS;
	}
	return I_VECTOR_DELETED;
}
int iQuit(bool bVectorCreated, int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements)
{
	if (bVectorCreated)
	{
		vDeleteVector(pitValues, pitOffsets, piBasicValueContainer, piLengthContainer, piNumberOfElements);
	}
	return I_QUIT_MESSAGE;
}
int iHelpMessage()
{
	vInstruction();
	return I_NO_ERROR;
}
int iChangeLength(bool bVectorCreated, int iLength, int &piLengthContainer, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength) 
{
	if (bVectorCreated)
	{
		if (iLength > 0)
		{
			vChangeVectorLength(iLength, piLengthContainer, pitValues, pitOffsets, piNumberOfElements, piTableLength);
		}
		else
		{
			return I_WRONG_ARGUMENTS;
		}
	}
	else
	{
		return I_VECTOR_NOT_EXISTS;
	}
	return I_LENGTH_CHANGED;
}
int iPrintValue(int &iPrintNumber, bool bVectorCreated, int iReadPos, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements,int iVectorLength)
{
	if (bVectorCreated)
	{
		if (iReadPos > 0 && iReadPos < iVectorLength)
		{
			iPrintNumber = iReadValue(iReadPos, iBasicValue, pitValues, pitOffsets, iNumberOfElements);
		}
		else
		{
			return I_WRONG_ARGUMENTS;
		}
	}
	else
	{
		return I_VECTOR_NOT_EXISTS;
	}
	return I_VALUE;
}
int iMakeVector(bool &bVectorCreated, int iLength, int iBasicValue, int &piLengthContainer, int &piBasicValueContainer, int **pitValues, int **pitOffsets, int &piTableLength, int &piNumberOfElements)
{
	if (iLength >= 0)
	{
		vCreateVector(iLength, iBasicValue, piLengthContainer, piBasicValueContainer, pitValues, pitOffsets, piTableLength, piNumberOfElements);
		if (bVectorCreated)
		{
			return I_DELETED_EXISTING_AND_CREATED;
		}
		else
		{
			bVectorCreated = true;
			return I_VECTOR_CREATED;
		}
	}
	else
	{
		return I_WRONG_ARGUMENTS;
	}
}
int iDefineValue(bool bVectorCreated, int iValue, int iOffset, int iBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int iVectorLength)
{
	if (bVectorCreated)
	{
		if (iOffset >= 0 && iOffset < iVectorLength)
		{
			vSetCertainValue(iValue, iOffset, iBasicValue, pitValues, pitOffsets, piNumberOfElements, piTableLength, iVectorLength);
		}
		else
		{
			return I_WRONG_ARGUMENTS;
		}
	}
	else
	{
		return I_VECTOR_NOT_EXISTS;
	}
	return I_VALUE_SET;
}