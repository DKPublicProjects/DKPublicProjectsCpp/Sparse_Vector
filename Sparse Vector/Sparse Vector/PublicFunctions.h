#pragma once

#include <string>
#include <iostream>
#include "MainAppFunctions.h"
#include "Const.h"


int iPrint(bool bVectorCreated, int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements);
int iDelete(bool &bVectorCreated, int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements);
int iQuit(bool bVectorCreated, int **pitValues, int **pitOffsets, int &piBasicValueContainer, int &piLengthContainer, int &piNumberOfElements);
int iHelpMessage();
int iChangeLength(bool bVectorCreated, int iLength, int &piLengthContainer, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength);
int iPrintValue(int &iPrintNumber, bool bVectorCreated, int iReadPos, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements, int iVectorLength);
int iMakeVector(bool &bVectorCreated, int iLength, int iBasicValue, int &piLengthContainer, int &piBasicValueContainer, int **pitValues, int **pitOffsets, int &piTableLength, int &piNumberOfElements);
int iDefineValue(bool bVectorCreated, int iValue, int iOffset, int iBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int iVectorLength);