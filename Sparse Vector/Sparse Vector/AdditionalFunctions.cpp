#include "AdditionalFunctions.h"


using namespace std;

void vClearVector(int **pitValues, int **pitOffsets)
{
	delete[] *pitValues;
	delete[] *pitOffsets;
}//clearVector: memory free
void vFillTables(int *pitValues, int *pitOffsets, int iTableLength)
{
	for (int i = 0; i < iTableLength; i++)
	{
		pitValues[i] = -1;
		pitOffsets[i] = -1;
	}
}
void vCreateTables(int **pitValues, int **pitOffsets, int iTableLength, int &piTableLengthContainer)
{
	*pitValues = new int[iTableLength];
	*pitOffsets = new int[iTableLength];
	piTableLengthContainer = iTableLength;
	vFillTables(*pitValues, *pitOffsets, piTableLengthContainer);
}
void vClearVectorTail(int iIndex, int *pitValues, int *pitOffsets, int &piNumberOfElements)
{
	for (int i = iIndex; i < piNumberOfElements; i++)
	{
		pitValues[i] = -1;
		pitOffsets[i] = -1;
		piNumberOfElements--;
	}

}
int iOffsetContains(int iOffset, int *pitOffsets, int numberOfElements)
{
	int i = 0;
	while (i < numberOfElements && pitOffsets[i] < iOffset)
	{
		i++;
	}
	if (pitOffsets[i] == iOffset)
	{
		return i;
	}
	else
	{
		return -1;
	}
}
string sVectorOnString(int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements)
{
	string s_result = S_LENGTH + to_string(iLength) + S_VALUE;
	int i_tableIterator = 0;
	for (int i = 0; i < iLength; i++)
	{
		i_tableIterator = iOffsetContains(i, pitOffsets, iNumberOfElements);
		if (i_tableIterator != -1)
		{
			s_result +=  to_string(pitValues[i_tableIterator]) + S_COMMA;
		}
		else
		{
			s_result += to_string(iBasicValue) + S_COMMA;
		}
	}
	return s_result.erase(s_result.length()-2);
}
void vChangeTableLength(int **pitValues, int **pitOffsets, int iNumberOfElements,int &piTableLength, bool bWid)
{
	if (bWid)//true -> widen  // false -> narrow
	{
		piTableLength *= I_DOUBLE_TABLE; //always double the TableLength
	}
	else
	{
		piTableLength /= I_DOUBLE_TABLE; 
	}
	int *pit_Values = new int[piTableLength];
	int *pit_Offsets = new int[piTableLength];
	for (int i = 0; i<iNumberOfElements; i++)
	{
		pit_Values[i] = (*pitValues)[i];
		pit_Offsets[i] = (*pitOffsets)[i];
	}
	vClearVector(pitValues, pitOffsets);
	*pitValues = pit_Values;
	*pitOffsets = pit_Offsets;
}
void vSet(int iIndex, int iValue, int iOffset, int *pitValues, int *pitOffsets, int &piNumberOfElements) 
{
	int iPreviousElement = 0;
	
	for (int i = piNumberOfElements; i > iIndex; i--)
	{
		iPreviousElement = i - 1;
		pitValues[i] = pitValues[iPreviousElement];
		pitOffsets[i] = pitOffsets[iPreviousElement];
	}
	pitValues[iIndex] = iValue;
	pitOffsets[iIndex] = iOffset;
}
void vAddSorted(int iValue, int iOffset, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength)
{
	int i = 0;
	piNumberOfElements++;
	if (piTableLength == piNumberOfElements)
	{
		vChangeTableLength(pitValues, pitOffsets, piNumberOfElements, piTableLength, true);
	}

	while (i < piNumberOfElements && (*pitOffsets)[i] < iOffset)
	{
		i++;
	}
	vSet(i, iValue, iOffset, *pitValues, *pitOffsets, piNumberOfElements);
}
void vDeleteElement(int iIndex, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength)
{
	int iNextElement = 0;
	piNumberOfElements--;
	for (int i = iIndex; i<piNumberOfElements; i++)
	{
		iNextElement = i + 1;
		pitValues[i] = pitValues[iNextElement];
		pitOffsets[i] = pitOffsets[iNextElement];
	}
	if (piTableLength > I_DOUBLE_TABLE * piNumberOfElements && piTableLength > I_BASIC_TABLE_LENGTH)
	{
		vChangeTableLength(&pitValues, &pitOffsets, piNumberOfElements, piTableLength, false);
	}
	pitValues[iNextElement] = -1;
	pitOffsets[iNextElement] = -1;
}

