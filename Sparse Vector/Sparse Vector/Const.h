#pragma once
//table
const int I_BASIC_TABLE_LENGTH = 10;	
const int I_DOUBLE_TABLE = 2;
//stringTableEnum
const int I_FUNCTION = 0;
const int I_ARG_1 = 1;
const int I_ARG_2 = 2;
//functions classification
const int I_NO_ARG_FUNCTIONS = 1;
const int I_ONE_ARG_FUNCTIONS = 2;
const int I_TWO_ARG_FUNCTIONS = 3;
//commands
const std::string S_MVEC = "mvec";
const std::string S_DEF = "def";
const std::string S_DEL = "del";
const std::string S_PRINT = "print";
const std::string S_LEN = "len";
const std::string S_QUIT = "quit";
const std::string S_GET = "get";
const std::string S_HELP = "help";
//Messages
const std::string S_VECTOR_NOT_EXIST = "Actually vector does not exist";
const std::string S_VECTOR_CREATED = "Sparse vector created";
const std::string S_VECTOR_DELETED = "Deleted existing vector";
const std::string S_CREATE_VECTOR = "Create vector first!";
const std::string S_QUIT_MESSAGE = "End of program";
const std::string S_HELP_END = "/help";
const std::string S_LENGTH_CHANGED = "Length changed!";
const std::string S_VALUE_SET = "Value stored";
//ErrorMessages
const std::string S_WRONG_COMMAND = "Wrong command, try again";
const std::string S_CHANGE_BASIC_VALUE_ERROR = "you cant change basic value if vector has any other values";
const std::string S_WRONG_ARGS = "Error, Arguments do not match the requirements";
//InterfaceInformationIDs
const int I_WRONG_COMMAND = -3;
const int I_WRONG_ARGUMENTS = -2;
const int I_VECTOR_NOT_EXISTS = -1;
const int I_NO_ERROR = 0;
const int I_VECTOR_DELETED = 1;
const int I_QUIT_MESSAGE = 2;
const int I_HELP = 3;
const int I_LENGTH_CHANGED = 4;
const int I_DELETED_EXISTING_AND_CREATED = 5;
const int I_VECTOR_CREATED = 6;
const int I_VALUE_SET = 7;
const int I_VALUE = 8;
//ASCII
const int I_ASCII_ZERO = 48;
const int I_ASCII_NINE = 57;
const int I_ASCII_MINUS = 45;
//Calculations
const int I_SHIFT_NUMBER = 10;
//Split
const int I_MAXIMAL_SPLIT_STRINGS = 3;
const std::string S_SPACE = " ";
//StringModifications
const std::string S_COMMA = ", ";
const std::string S_LENGTH = "len: ";
const std::string S_VALUE = "val: ";