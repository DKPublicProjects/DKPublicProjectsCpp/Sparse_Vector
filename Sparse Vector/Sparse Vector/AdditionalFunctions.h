#pragma once
#include <iostream>
#include "Const.h"
#include "MainAppFunctions.h"
#include "AdditionalFunctions.h"
#include <string>

void vClearVector(int **pitValues, int **pitOffsets);
void vFillTables(int *pitValues, int *pitOffsets, int iTableLength);
void vCreateTables(int **pitValues, int **pitOffsets, int iTableLength, int &piTableLengthContainer);
void vClearVectorTail(int iIndex, int *pitValues, int *pitOffsets,int &piNumberOfElements);
int iOffsetContains(int iOffset, int *pitOffsets, int numberOfElements);
std::string sVectorOnString(int iLength, int iBasicValue, int *pitValues, int *pitOffsets, int iNumberOfElements);
void vChangeTableLength(int **pitValues, int **pitOffsets, int iNumberOfElements, int &piTableLength, bool bWid);
void vSet(int iIndex, int iValue, int iOffset, int *pitValues, int *pitOffsets, int &piNumberOfElements);
void vAddSorted(int iValue, int iOffset, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength);
void vDeleteElement(int iIndex, int *pitValues, int *pitOffsets, int &piNumberOfElements, int &piTableLength);