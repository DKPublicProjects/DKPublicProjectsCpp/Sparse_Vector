#pragma once
#include "PublicFunctions.h"
#include <iostream>
#include <string>
#include "Const.h"
#include "Interface.h"

int iSplitString(std::string sContainer, std::string *psSplitTable);
int getArg(std::string sArgContainer, int &iValid);
void vSendMessage(int iMessageId, int iPrintNumber);
bool bNoArgFunctions(bool &bVectorCreated, std::string *pstSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId);//return boolean to differ when program should end
void vOneArgFunctions(int &iPrintNumber, bool &bVectorCreated, std::string *stSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId);
void vTwoArgFunctions(bool &bVectorCreated, std::string *stSplited, int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength, int &iMessageId);
void vCommandLineInterface(int &piVectorLength, int &piBasicValue, int **pitValues, int **pitOffsets, int &piNumberOfElements, int &piTableLength);
void vRun();
